#!/usr/bin/perl
@libs = (
"libSDL-1.3.0.dylib",
"libSDL_image-1.2.0.dylib",
"libpng15.dylib"
);

$origLibDir = "/usr/local/lib";
$origLibDir2 = "/usr/local//lib"; # stupid double-slashes
$appBinary = "./maxx";
$frameworks = ".";
$frameworksRef = "\@executable_path";

sub mysystem($)
{
	$cmd = shift;
	#print("$cmd\n");
	system("$cmd");
}

mysystem("mkdir -p $frameworks");

foreach $lib (@libs)
{
	mysystem("cp $origLibDir/$lib $frameworks");
	mysystem("install_name_tool -id $frameworksRef/$lib $frameworks/$lib");
	mysystem("install_name_tool -change $origLibDir/$lib $frameworksRef/$lib $appBinary");
	mysystem("install_name_tool -change $origLibDir2/$lib $frameworksRef/$lib $appBinary");
	foreach $lib2 (@libs)
	{
		mysystem("install_name_tool -change $origLibDir/$lib2 $frameworksRef/$lib2 $frameworks/$lib");
		mysystem("install_name_tool -change $origLibDir2/$lib2 $frameworksRef/$lib2 $frameworks/$lib");
	}
}
