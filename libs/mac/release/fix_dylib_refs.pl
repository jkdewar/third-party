#!/usr/bin/perl
@libs = (
"libSDL-1.3.0.dylib",
"libSDL_image-1.2.0.dylib",
"libSDL_ttf-2.0.0.dylib",
"libfreetype.6.dylib",
"libiconv.2.dylib",
);

sub mysystem($)
{
	$cmd = shift;
	print("$cmd\n");
	system("$cmd");
}

foreach $lib (@libs)
{
	mysystem("install_name_tool -id \@loader_path/$lib $lib");
	foreach $lib2 (@libs)
	{
		mysystem("install_name_tool -change \@executable_path/$lib \@loader_path/$lib $lib2");
	}
}
